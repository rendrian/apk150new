<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LinkAgenPromo extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('link_agenpromo', function (Blueprint $table) {
            $table->string('id');
            $table->string('judul');
            $table->string('image')->nullable();
            $table->text('kalimat')->nullable();
            $table->json('urls')->nullable();

            $table->timestamps();
            $table->integer('position')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('link_agenpromo');
    }
}
