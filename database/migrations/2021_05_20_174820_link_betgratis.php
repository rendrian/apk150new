<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LinkBetgratis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('link_betgratis', function (Blueprint $table) {
            $table->string('id');
            $table->string('judul');
            $table->string('image')->nullable();
            $table->text('kalimat')->nullable();
            $table->json('urls')->nullable();

            $table->timestamps();
            $table->integer('position')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('link_betgratis');
    }
}
