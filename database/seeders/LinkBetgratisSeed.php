<?php

namespace Database\Seeders;

use App\Models\LinkBetgratis;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LinkBetgratisSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = $this->oldData();

        foreach ($data as $val) {
            $insert = array(
                'id' => $val->id,
                'judul' => $val->judul,
                'image' => $val->image,
                'kalimat' => $val->kalimat,
                'urls' => json_encode($val->urls),
                'position' => $val->position,
            );
            DB::table('link_betgratis')->insert($insert);
            //


            // dd($val->urls);
        }
    }


    private function oldData()
    {
        $json = '[
            {
              "id": 479,
              "judul": "FREEBET GRATIS RP 10.000 RCASH88",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-rcash88/"
              ],
              "created_at": "2021-03-29 18:24:57",
              "updated_at": "2021-05-20 15:26:09",
              "position": 39
            },
            {
              "id": 484,
              "judul": "FREEBET GRATIS RP 20.000 MACAUDEWA",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-macaudewa/"
              ],
              "created_at": "2021-04-01 21:34:49",
              "updated_at": "2021-05-20 15:26:09",
              "position": 37
            },
            {
              "id": 505,
              "judul": "FREEBET GRATIS RP 10.000 QQ288",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-qq288/"
              ],
              "created_at": "2021-04-14 21:11:11",
              "updated_at": "2021-05-20 15:26:09",
              "position": 84
            },
            {
              "id": 506,
              "judul": "FREEBET GRATIS RP 10.000 QQ101",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-qq101/"
              ],
              "created_at": "2021-04-14 21:19:46",
              "updated_at": "2021-05-20 15:26:09",
              "position": 83
            },
            {
              "id": 507,
              "judul": "FREEBET GRATIS RP 10.000 QQ828",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-qq828/"
              ],
              "created_at": "2021-04-14 21:26:33",
              "updated_at": "2021-05-20 15:26:09",
              "position": 82
            },
            {
              "id": 508,
              "judul": "FREEBET GRATIS RP 10.000 QQ808",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/qq808-freebet-gratis-rp-10-000-validasi-hp/"
              ],
              "created_at": "2021-04-14 21:34:15",
              "updated_at": "2021-05-20 15:26:09",
              "position": 81
            },
            {
              "id": 509,
              "judul": "FREEBET GRATIS RP 10.000 QQ724",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-qq724/"
              ],
              "created_at": "2021-04-14 21:41:13",
              "updated_at": "2021-05-20 15:26:09",
              "position": 80
            },
            {
              "id": 511,
              "judul": "FREEBET GRATIS RP 10.000 QQ8889",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "urls": [
                "https://157.245.199.80/freebet-gratis-qq8889/"
              ],
              "created_at": "2021-04-14 21:49:55",
              "updated_at": "2021-05-20 15:26:09",
              "position": 79
            },
            {
              "id": 512,
              "judul": "FREEBET GRATIS RP 10.000 QQ882",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-qq882/"
              ],
              "created_at": "2021-04-14 22:08:27",
              "updated_at": "2021-05-20 15:26:09",
              "position": 78
            },
            {
              "id": 513,
              "judul": "FREEBET GRATIS RP 10.000 QQ1X2",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-qq1x2/"
              ],
              "created_at": "2021-04-14 22:08:46",
              "updated_at": "2021-05-20 15:26:09",
              "position": 77
            },
            {
              "id": 514,
              "judul": "FREEBET GRATIS RP 10.000 QQ188",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-qq188/"
              ],
              "created_at": "2021-04-14 22:19:00",
              "updated_at": "2021-05-20 15:26:09",
              "position": 76
            },
            {
              "id": 515,
              "judul": "FREEBET GRATIS RP 10.000 QQ801",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-qq801/"
              ],
              "created_at": "2021-04-14 22:27:35",
              "updated_at": "2021-05-20 15:26:09",
              "position": 75
            },
            {
              "id": 516,
              "judul": "FREEBET GRATIS RP 10.000 QQ820",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-qq820/"
              ],
              "created_at": "2021-04-14 22:33:54",
              "updated_at": "2021-05-20 15:26:09",
              "position": 74
            },
            {
              "id": 517,
              "judul": "FREEBET GRATIS RP 10.000 HORE55",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-hore55/"
              ],
              "created_at": "2021-04-14 22:45:44",
              "updated_at": "2021-05-20 15:26:09",
              "position": 73
            },
            {
              "id": 518,
              "judul": "FREEBET GRATIS RP 10.000 MPOXL",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-mpoxl/"
              ],
              "created_at": "2021-04-15 22:16:42",
              "updated_at": "2021-05-20 15:26:09",
              "position": 72
            },
            {
              "id": 520,
              "judul": "FREEBET GRATIS RP 10.000 HSCBET",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-hscbet/"
              ],
              "created_at": "2021-04-15 22:27:11",
              "updated_at": "2021-05-20 15:26:09",
              "position": 71
            },
            {
              "id": 521,
              "judul": "FREEBET GRATIS RP 10.000 HSCBET",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-hscbet/"
              ],
              "created_at": "2021-04-15 22:27:11",
              "updated_at": "2021-05-20 15:26:09",
              "position": 70
            },
            {
              "id": 523,
              "judul": "FREEBET GRATIS TERBARU RP 25.000 GAG88",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-gag88/"
              ],
              "created_at": "2021-04-15 23:20:38",
              "updated_at": "2021-05-20 15:26:09",
              "position": 8
            },
            {
              "id": 524,
              "judul": "FREEBET GRATIS RP 10.000 RMSLOT",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-rmslot/"
              ],
              "created_at": "2021-04-16 16:43:41",
              "updated_at": "2021-05-20 15:26:09",
              "position": 69
            },
            {
              "id": 525,
              "judul": "FREEBET GRATIS RP 10.000 GA PAKAI RIBET YBPLAY",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-ybplay/"
              ],
              "created_at": "2021-04-17 20:04:25",
              "updated_at": "2021-05-20 15:26:09",
              "position": 68
            },
            {
              "id": 526,
              "judul": "FREEBET GRATIS RP 15.000 TERBARU GRUP123",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-grup123/"
              ],
              "created_at": "2021-04-18 05:41:02",
              "updated_at": "2021-05-20 15:26:09",
              "position": 67
            },
            {
              "id": 527,
              "judul": "FREEBET GRATIS RP 10.000 TERBARU HOKIVEGAS",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-hokivegas/"
              ],
              "created_at": "2021-04-18 14:17:34",
              "updated_at": "2021-05-20 15:26:09",
              "position": 66
            },
            {
              "id": 528,
              "judul": "FREEBET GRATIS RP 10.000 MAXPRO88",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-maxpro88/"
              ],
              "created_at": "2021-04-18 20:13:11",
              "updated_at": "2021-05-20 15:26:09",
              "position": 65
            },
            {
              "id": 529,
              "judul": "FREEBET GRATIS RP 20.000 ASIXSLOT",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-asixslot/"
              ],
              "created_at": "2021-04-19 13:10:00",
              "updated_at": "2021-05-20 15:26:09",
              "position": 64
            },
            {
              "id": 530,
              "judul": "FREEBET GRATIS RP 10.000 TERBARU OTWSLOT",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-otwslot/"
              ],
              "created_at": "2021-04-19 19:42:11",
              "updated_at": "2021-05-20 15:26:09",
              "position": 63
            },
            {
              "id": 531,
              "judul": "FREEBET GRATIS 18HOKI",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-18hoki/"
              ],
              "created_at": "2021-04-21 16:15:29",
              "updated_at": "2021-05-20 15:26:09",
              "position": 62
            },
            {
              "id": 532,
              "judul": "FREEBET GRATIS RP 10.000 SBOTSLOT99",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-sboslot99/"
              ],
              "created_at": "2021-04-21 16:24:32",
              "updated_at": "2021-05-20 15:26:09",
              "position": 61
            },
            {
              "id": 533,
              "judul": "FREEBET GRATIS RP 20.000 TERBARU MANGGA2BET",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-mangga2bet/"
              ],
              "created_at": "2021-04-21 16:36:13",
              "updated_at": "2021-05-20 15:26:09",
              "position": 60
            },
            {
              "id": 534,
              "judul": "FREEBET GRATIS RP 15.000 WARUNG168",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-warung168/"
              ],
              "created_at": "2021-04-23 16:14:41",
              "updated_at": "2021-05-20 15:26:09",
              "position": 59
            },
            {
              "id": 535,
              "judul": "FREEBET GRATIS RP 10.0000 JUDIKARTU",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-judikartu/"
              ],
              "created_at": "2021-04-23 18:08:29",
              "updated_at": "2021-05-20 15:26:09",
              "position": 58
            },
            {
              "id": 536,
              "judul": "FREEBET GRATIS RP 10.000 IAOBET",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-iaobet/"
              ],
              "created_at": "2021-04-23 18:21:00",
              "updated_at": "2021-05-20 15:26:09",
              "position": 57
            },
            {
              "id": 538,
              "judul": "FREEBET GRATIS RP 10.000 KADOBET",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-kadobet/"
              ],
              "created_at": "2021-04-23 18:51:39",
              "updated_at": "2021-05-20 15:26:09",
              "position": 56
            },
            {
              "id": 539,
              "judul": "FREEBET GRATIS RP 10.000 TERBARU APPLE4D",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-apple4d/"
              ],
              "created_at": "2021-04-24 01:17:04",
              "updated_at": "2021-05-20 15:26:09",
              "position": 55
            },
            {
              "id": 540,
              "judul": "FREEBET GRATIS RP 10.000 JIMSLOT",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-jimslot/"
              ],
              "created_at": "2021-04-24 21:01:38",
              "updated_at": "2021-05-20 15:26:09",
              "position": 54
            },
            {
              "id": 541,
              "judul": "FREEBET GRATIS RP 15.000 HOKI188",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-hoki188/"
              ],
              "created_at": "2021-04-25 00:36:55",
              "updated_at": "2021-05-20 15:26:09",
              "position": 53
            },
            {
              "id": 542,
              "judul": "FREEBET GRATIS RP 20.000 MACO4D",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-maco4d/"
              ],
              "created_at": "2021-04-25 15:44:25",
              "updated_at": "2021-05-20 15:26:09",
              "position": 52
            },
            {
              "id": 543,
              "judul": "FREEBET GRATIS RP 10.000 TERBARU USAKLUB",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-usaklub/"
              ],
              "created_at": "2021-04-26 01:15:39",
              "updated_at": "2021-05-20 15:26:09",
              "position": 51
            },
            {
              "id": 544,
              "judul": "FREEBET GRATIS RP 10.000 TERBARU WINCASINO365",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-wincasino365/"
              ],
              "created_at": "2021-04-27 01:27:02",
              "updated_at": "2021-05-20 15:26:09",
              "position": 50
            },
            {
              "id": 545,
              "judul": "FREEBET GRATIS TERBARU RP 15.000 LIGATOTO",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-ligatoto/"
              ],
              "created_at": "2021-04-27 01:34:38",
              "updated_at": "2021-05-20 15:26:09",
              "position": 49
            },
            {
              "id": 546,
              "judul": "FREEBET GRATIS RP 10.000 S7SLOT",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-s7slot/"
              ],
              "created_at": "2021-04-27 19:57:05",
              "updated_at": "2021-05-20 15:26:09",
              "position": 48
            },
            {
              "id": 547,
              "judul": "FREEBET GRATIS RP 15.000 SKY77",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-sky77/"
              ],
              "created_at": "2021-04-28 00:35:42",
              "updated_at": "2021-05-20 15:26:09",
              "position": 47
            },
            {
              "id": 548,
              "judul": "FREEBET GRATIS RP 15.000 AGEN78",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-agen78/"
              ],
              "created_at": "2021-04-28 19:40:42",
              "updated_at": "2021-05-20 15:26:09",
              "position": 46
            },
            {
              "id": 549,
              "judul": "FREEBET GRATIS RP 10.000 MURAHSLOT",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-murahslot/"
              ],
              "created_at": "2021-04-28 19:51:02",
              "updated_at": "2021-05-20 15:26:09",
              "position": 45
            },
            {
              "id": 550,
              "judul": "FREEBET GRATIS RP 15.000 HYDRO88",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-hydro88/"
              ],
              "created_at": "2021-04-29 16:09:32",
              "updated_at": "2021-05-20 15:26:09",
              "position": 44
            },
            {
              "id": 551,
              "judul": "FREEBET GRATIS RP 10.000 KINGBET188",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-kingbet188/"
              ],
              "created_at": "2021-04-29 17:12:55",
              "updated_at": "2021-05-20 15:26:09",
              "position": 43
            },
            {
              "id": 552,
              "judul": "FREEBET GRATIS RP 25.000 KERASAKTISLOT",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-kerasaktislot/"
              ],
              "created_at": "2021-04-29 23:20:36",
              "updated_at": "2021-05-20 15:26:09",
              "position": 42
            },
            {
              "id": 553,
              "judul": "FREEBET GRATIS RP 10.000 MUTU777",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-mutu777/"
              ],
              "created_at": "2021-04-30 17:13:18",
              "updated_at": "2021-05-20 15:26:09",
              "position": 41
            },
            {
              "id": 554,
              "judul": "FREEBET GRATIS RP 10.000 CNCBET",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-cncbet/"
              ],
              "created_at": "2021-05-01 03:48:57",
              "updated_at": "2021-05-20 15:26:09",
              "position": 40
            },
            {
              "id": 555,
              "judul": "FREEBET GRATIS RP 10.000 IDN168",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-idn168/"
              ],
              "created_at": "2021-05-01 16:31:17",
              "updated_at": "2021-05-20 15:26:09",
              "position": 38
            },
            {
              "id": 556,
              "judul": "FREEBET GRATIS RP 10.000 TERBARU BONASLOT",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-bonaslot/"
              ],
              "created_at": "2021-05-01 16:50:44",
              "updated_at": "2021-05-20 15:26:09",
              "position": 36
            },
            {
              "id": 557,
              "judul": "FREEBET GRATIS TERBARU RP 10.000 ZANDOPLAY",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-zandoplay/"
              ],
              "created_at": "2021-05-01 22:50:53",
              "updated_at": "2021-05-20 15:26:09",
              "position": 35
            },
            {
              "id": 558,
              "judul": "FREEBET GRATIS RP 10.000 IBOSPORT",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/ibosport-com-freebet-gratis-tanpa-deposit/"
              ],
              "created_at": "2021-05-03 06:51:39",
              "updated_at": "2021-05-20 15:26:09",
              "position": 34
            },
            {
              "id": 559,
              "judul": "FREEBET GRATIS RP 10.000 BANDAR36",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-bd36/"
              ],
              "created_at": "2021-05-03 06:51:55",
              "updated_at": "2021-05-20 15:26:09",
              "position": 33
            },
            {
              "id": 560,
              "judul": "FREEBET GRATIS TERBARU RP 10.000 HARIMAUMAIN",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-harimaumain/"
              ],
              "created_at": "2021-05-03 23:52:08",
              "updated_at": "2021-05-20 15:26:09",
              "position": 32
            },
            {
              "id": 561,
              "judul": "FREEBET GRATIS RP 15.000 TERBARU G9KING",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-g9king/"
              ],
              "created_at": "2021-05-04 00:01:08",
              "updated_at": "2021-05-20 15:26:09",
              "position": 31
            },
            {
              "id": 562,
              "judul": "FREEBET GRATIS RP 15.000 PLAYWIN123",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-playwin123/"
              ],
              "created_at": "2021-05-04 00:08:35",
              "updated_at": "2021-05-20 15:26:09",
              "position": 30
            },
            {
              "id": 563,
              "judul": "FREEBET GRATIS RP 50.000 TANGKAS",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-tangkas/"
              ],
              "created_at": "2021-05-04 17:45:40",
              "updated_at": "2021-05-20 15:26:09",
              "position": 29
            },
            {
              "id": 564,
              "judul": "FREEBET GRATIS RP 10.000 MPO1881",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-mpo1881/"
              ],
              "created_at": "2021-05-04 17:54:54",
              "updated_at": "2021-05-20 15:26:09",
              "position": 28
            },
            {
              "id": 565,
              "judul": "FREEBET GRATIS RP 10.000 SLOT138",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-slot138/"
              ],
              "created_at": "2021-05-05 07:46:40",
              "updated_at": "2021-05-20 15:26:09",
              "position": 27
            },
            {
              "id": 566,
              "judul": "FREEBET GRATIS RP 15.000 YBPLAY",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-ybplay/"
              ],
              "created_at": "2021-05-05 16:45:54",
              "updated_at": "2021-05-20 15:26:09",
              "position": 26
            },
            {
              "id": 567,
              "judul": "FREEBET GRATIS RP 10.000 TERBARU MPO1771",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-mpo1771/"
              ],
              "created_at": "2021-05-05 16:54:59",
              "updated_at": "2021-05-20 15:26:09",
              "position": 25
            },
            {
              "id": 568,
              "judul": "FREEBET GRATIS RP 10.000 MPOTOWER",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-mpotower/"
              ],
              "created_at": "2021-05-05 19:08:41",
              "updated_at": "2021-05-20 15:26:09",
              "position": 24
            },
            {
              "id": 569,
              "judul": "FREEBET GRATIS RP 10.000 MPO1551",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-mpo1551/"
              ],
              "created_at": "2021-05-05 19:15:25",
              "updated_at": "2021-05-20 15:26:09",
              "position": 23
            },
            {
              "id": 570,
              "judul": "FREEBET GRATIS RP 20.000 BOSSWIN168",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-bosswin168/"
              ],
              "created_at": "2021-05-06 17:47:05",
              "updated_at": "2021-05-20 15:26:09",
              "position": 22
            },
            {
              "id": 571,
              "judul": "FREEBET GRATIS RP 15.000 INDOSLOTGAMING",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-indoslotgaming/"
              ],
              "created_at": "2021-05-07 17:31:58",
              "updated_at": "2021-05-20 15:26:09",
              "position": 21
            },
            {
              "id": 572,
              "judul": "FREEBET GRATIS RP 10.000 LONDONKLUB",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-londonklub/"
              ],
              "created_at": "2021-05-07 17:39:39",
              "updated_at": "2021-05-20 15:26:09",
              "position": 20
            },
            {
              "id": 573,
              "judul": "FREEBET GRATIS RP 10.000 PLANET88",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-planet88/"
              ],
              "created_at": "2021-05-08 22:26:24",
              "updated_at": "2021-05-20 15:26:09",
              "position": 19
            },
            {
              "id": 574,
              "judul": "FREEBET GRATIS RP 10.000 BANDAR55",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-bandar55/"
              ],
              "created_at": "2021-05-08 22:53:38",
              "updated_at": "2021-05-20 15:26:09",
              "position": 18
            },
            {
              "id": 575,
              "judul": "FREEBET GRATIS RP 10.000 LARISTOTO",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-laristoto/"
              ],
              "created_at": "2021-05-09 11:46:58",
              "updated_at": "2021-05-20 15:26:09",
              "position": 17
            },
            {
              "id": 576,
              "judul": "FREEBET GRATIS RP 50.000 QQ1220",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-qq1220/"
              ],
              "created_at": "2021-05-09 12:00:30",
              "updated_at": "2021-05-20 15:26:09",
              "position": 16
            },
            {
              "id": 577,
              "judul": "FREEBET GRATIS RP 18.000 ACEVIP",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-acevip/"
              ],
              "created_at": "2021-05-10 18:20:54",
              "updated_at": "2021-05-20 15:26:09",
              "position": 15
            },
            {
              "id": 578,
              "judul": "FREEBET GRATIS RP 10.000 ACONG4D",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "urls": [
                "https://157.245.199.80/freebet-gratis-acong4d/"
              ],
              "created_at": "2021-05-10 22:38:52",
              "updated_at": "2021-05-20 15:26:09",
              "position": 14
            },
            {
              "id": 579,
              "judul": "FREEBET GRATIS RP 15.000 PLAYSLOT123",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/playslot123-freebet-gratis-rp-15-000-tanpa-deposit/"
              ],
              "created_at": "2021-05-11 13:20:58",
              "updated_at": "2021-05-20 15:26:09",
              "position": 13
            },
            {
              "id": 580,
              "judul": "FREEBET GRATIS RP 30.000 USLOT88",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-uslot88/"
              ],
              "created_at": "2021-05-12 02:31:32",
              "updated_at": "2021-05-20 15:26:09",
              "position": 12
            },
            {
              "id": 581,
              "judul": "FREEBET GRATIS RP 10.000 MACAUKLUB",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-macauklub/"
              ],
              "created_at": "2021-05-12 23:14:03",
              "updated_at": "2021-05-20 15:26:09",
              "position": 11
            },
            {
              "id": 582,
              "judul": "FREEBET GRATIS RP 20.000 WINCASINO",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-wnicasino/"
              ],
              "created_at": "2021-05-13 15:23:55",
              "updated_at": "2021-05-20 15:26:09",
              "position": 10
            },
            {
              "id": 583,
              "judul": "FREEBET GRATIS RP 10.000 ABGTOP",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-abgtop/"
              ],
              "created_at": "2021-05-13 21:23:57",
              "updated_at": "2021-05-20 15:26:09",
              "position": 9
            },
            {
              "id": 584,
              "judul": "FREEBET GRATIS RP 10.000 SGTOTO",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-sgtoto/"
              ],
              "created_at": "2021-05-16 11:04:58",
              "updated_at": "2021-05-20 15:26:09",
              "position": 7
            },
            {
              "id": 585,
              "judul": "FREEBET GRATIS RP 15.000 IBC138",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-ibc138/"
              ],
              "created_at": "2021-05-18 10:58:20",
              "updated_at": "2021-05-20 15:26:09",
              "position": 6
            },
            {
              "id": 586,
              "judul": "FREEBET GRATIS RP 25.000 TEMPOSLOT",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-temposlot/"
              ],
              "created_at": "2021-05-18 23:43:13",
              "updated_at": "2021-05-20 15:26:09",
              "position": 5
            },
            {
              "id": 587,
              "judul": "FREEBET GRATIS RP 10.000 MERCY88",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-mercy88/"
              ],
              "created_at": "2021-05-19 17:57:59",
              "updated_at": "2021-05-20 15:26:09",
              "position": 4
            },
            {
              "id": 588,
              "judul": "FREEBET GRATIS RP 20.000 MPO633",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-mpo633/"
              ],
              "created_at": "2021-05-20 03:44:24",
              "updated_at": "2021-05-20 15:26:09",
              "position": 3
            },
            {
              "id": 589,
              "judul": "FREEBET GRATIS RP 20.000 BEST188",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-best188/"
              ],
              "created_at": "2021-05-20 04:16:38",
              "updated_at": "2021-05-20 15:26:09",
              "position": 2
            },
            {
              "id": 590,
              "judul": "FREEBET GRATIS RP 20.000 DEWA898",
              "image": "https://1.bp.blogspot.com/-wRITZL_Y6NU/XmC7qMcdrjI/AAAAAAAAJa0/-XNK-tDWEvwcM-Rm-GIRuNtiS6Xd2rHEQCLcBGAsYHQ/s640/99cash20k.jpg",
              "kalimat": "Cek Info Freebetnya KLIK Link Dibawah:",
              "urls": [
                "https://157.245.199.80/freebet-gratis-dewa898/"
              ],
              "created_at": "2021-05-20 15:26:09",
              "updated_at": "2021-05-20 15:26:09",
              "position": 1
            }
          ]';


        return json_decode($json);
    }
}
