<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // check if admin exist
        User::truncate();
        DB::table('users')->insert([
            "id" => Str::uuid(),
            "name" => 'admin',
            "email" => 'admin@gmail.com',
            "password" => Hash::make('123'),
            "level" => "admin"
        ]);
    }
}
