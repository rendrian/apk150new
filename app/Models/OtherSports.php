<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OtherSports extends Model
{
    protected $table = 'other_sports';
    protected $fillable = [
        'id',
        'name',
        'logo',
        'sport_id',
    ];
    protected $primaryKey = 'id'; // or null
    public $incrementing = false;
}
