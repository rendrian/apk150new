<?php

use App\Models\SportMatch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

if (!function_exists('getTeamList')) {
    function getTeamList($sportname)
    {
        return DB::table('teams')->where('sport', $sportname)->get();
    }
}

if (!function_exists('getMatchList')) {
    function getMatchList($sportname)
    {
        return SportMatch::with(['homedata', 'awaydata'])->where('sport', $sportname)
            ->where('is_request', false)
            ->orderBy('position', 'asc')->get();
        // die;
        return DB::table('match')->where('sport', $sportname)->get();
    }
}

if (!function_exists('saveMatch')) {
    function saveMatch($data)
    {
        try {
            $id = Str::slug($data['home'].' vs '.$data['away']);
            $randomStr = Str::random(7);
            $new = new SportMatch();
            $newdata = [
                'id' => $randomStr,
                'slug' => $id,
                'liga' => $data['liga'],
                'home' => $data['home'],
                'away' => $data['away'],
                'is_request' => $data['is_request'],
                'sport' => $data['sport'],
                'sources' => $data['sources'],
                'position' => $data['position'],
                // 'sources' => json_encode($data['sources'])
            ];

            $new->fill($newdata);
            $new->save();

            $jsonData = SportMatch::with('homedata', 'awaydata')->find($randomStr);

            saveJson($randomStr, $jsonData);

            saveSport($data['sport']);

            return true;
        } catch (\Throwable $th) {
            // dd($th);
            Log::error('Insert Match failed, msg : '.$th->getMessage());

            return false;
        }
    }
}
if (!function_exists('updateMatch')) {
    function updateMatch($id, $data)
    {
        try {
            $slug = Str::slug($data['home'].' vs '.$data['away']);
            $randomStr = $id;
            $updatedata = [
                'slug' => $slug,
                'liga' => $data['liga'],
                'home' => $data['home'],
                'away' => $data['away'],
                'sport' => $data['sport'],
                'is_request' => $data['is_request'],
                'sources' => json_encode($data['sources']),
            ];

            SportMatch::where('id', $id)
                ->update($updatedata)
            ;

            /**
             * save all sport when not request.
             */
            $jsonData = SportMatch::with('homedata', 'awaydata')->find($id);

            saveSport($data['sport']);
            saveJson($id, $jsonData);

            return true;
        } catch (\Throwable $th) {
            // dd($th);
            Log::error('Insert Match failed, msg : '.$th->getMessage());

            return false;
        }
    }
}

if (!function_exists('getBlockDataById')) {
    function getBlockDataById($type, $id)
    {
        return DB::table('block_'.$type)->where('id', $id)->first();
    }
}
if (!function_exists('getBlockListData')) {
    function getBlockListData($type)
    {
        return DB::table('block_'.$type)->get();
    }
}
if (!function_exists('writeBlock')) {
    function writeBlock($type)
    {
        $allData = DB::table('block_'.$type)->get()->toArray();
        $saveBlock = array_column($allData, 'string');
        $saveBlock = array_unique($saveBlock);
        $saveBlock = array_values($saveBlock);
        saveJson('block'.$type, $saveBlock);
    }
}
if (!function_exists('saveJson')) {
    function saveJson($filename, $data)
    {
        file_put_contents(public_path().'/streamdata/'.$filename.'.json', json_encode($data));
    }
}

if (!function_exists('getTeam')) {
    function getTeam($id, $sport)
    {
        try {
            return DB::table('teams')
                ->where('id', $id)
                ->where('sport', $sport)
                ->first()
            ;
        } catch (\Throwable $th) {
            Log::error('Get team info failed, msg : '.$th->getMessage());

            return false;
        }
    }
}

if (!function_exists('destroyLinkBetgratis')) {
    function destroyLinkBetgratis($id)
    {
        try {
            $destroy = DB::table('link_betgratis')
                ->where([
                    'id' => $id,
                ])
                ->delete()
            ;

            return true;
        } catch (\Throwable $th) {
            Log::error('Delete link betgratis failed, msg : '.$th->getMessage());

            return false;
        }
    }
}

if (!function_exists('destroyLinkAgenpromo')) {
    function destroyLinkAgenpromo($id)
    {
        try {
            $destroy = DB::table('link_agenpromo')
                ->where([
                    'id' => $id,
                ])
                ->delete()
            ;

            return true;
        } catch (\Throwable $th) {
            Log::error('Delete link agentpromo failed, msg : '.$th->getMessage());

            return false;
        }
    }
}

if (!function_exists('destroyTeam')) {
    function destroyTeam($id, $sport)
    {
        try {
            DB::table('teams')
                ->where([
                    'id' => $id,
                    'sport' => $sport,
                ])
                ->delete()
            ;

            return true;
        } catch (\Throwable $th) {
            Log::error('Delete team failed, msg : '.$th->getMessage());

            return false;
        }
    }
}
if (!function_exists('destroyOtherSport')) {
    function destroyOtherSport($id)
    {
        try {
            DB::table('other_sports')
                ->where([
                    'id' => $id,
                ])
                ->delete()
            ;

            writeOtherSport();

            return true;
        } catch (\Throwable $th) {
            Log::error('Delete other sport failed, msg : '.$th->getMessage());

            return false;
        }
    }
}

if (!function_exists('writeOtherSport')) {
    function writeOtherSport()
    {
        $allData = DB::table('other_sports')->orderBy('position', 'asc')->get();

        saveJson('othersports', $allData);
    }
}

if (!function_exists('destroyBlock')) {
    function destroyBlock($type, $id)
    {
        try {
            $destroy = DB::table('block_'.$type)
                ->where([
                    'id' => $id,
                ])
                ->delete()
            ;

            writeBlock($type);

            return true;
        } catch (\Throwable $th) {
            Log::error("Delete block {$type} failed, msg : ".$th->getMessage());

            return false;
        }
    }
}
if (!function_exists('saveSportRequest')) {
    function saveSportRequest()
    {
        $data = SportMatch::with(['homedata', 'awaydata'])->where('is_request', true)->get();
        saveJson('request', $data);
    }
}
if (!function_exists('saveSport')) {
    function saveSport($sport)
    {
        // dd($sport);
        saveSportRequest();
        $data = getMatchList($sport);

        saveJson($sport, $data);
    }
}
if (!function_exists('destroyMatch')) {
    function destroyMatch($id, $sport)
    {
        try {
            $destroy = DB::table('match')
                ->where([
                    'id' => $id,
                    'sport' => $sport,
                ])
                ->delete()
            ;

            saveSport($sport);
            $file = public_path().'/streamdata/'.$id.'.json';

            if (file_exists($file)) {
                unlink($file);
            }

            return true;
        } catch (\Throwable $th) {
            Log::error('Delete team failed, msg : '.$th->getMessage());

            return false;
        }
    }
}

if (!function_exists('addTeam')) {
    function addTeam($data)
    {
        try {
            DB::table('teams')
                ->insert([
                    'id' => Str::slug($data['name']),
                    'name' => $data['name'],
                    'logo' => $data['logo'],
                    'sport' => $data['sport'],
                ])
            ;

            return true;
        } catch (\Throwable $th) {
            Log::error('Add team error, info : '.$th->getMessage());

            return false;
        }
    }
}

if (!function_exists('updateTeam')) {
    function updateTeam($data)
    {
        try {
            DB::table('teams')
                ->where('id', $data['id'])
                ->where('sport', $data['sport'])
                ->update(
                    [
                        'name' => $data['name'],
                        'logo' => $data['logo'],
                    ]
                )
            ;

            return true;
        } catch (\Throwable $th) {
            Log::error('Save team error, info : '.$th->getMessage());

            return false;
        }
    }
}

if (!function_exists('getSportMenu')) {
    function getSportMenu()
    {
        $menuList = [
            [
                'title' => 'Football',
                'slug' => 'football',
                'route' => 'admin.sportdata',
                'logo' => 'fas fa-futbol',
                'sub' => [
                    [
                        'name' => 'indexTeam',
                        'title' => 'Team',
                        'slug' => 'football',
                        'logo' => 'fas fa-users',
                        'route' => 'admin.sportdata.listTeam',
                    ],
                    [
                        'name' => 'indexMatch',
                        'title' => 'Match',
                        'slug' => 'football',
                        'logo' => 'fas fa-calendar-alt',
                        'route' => 'admin.sportdata.listMatch',
                    ],
                ],
            ],
            [
                'title' => 'Basketball',
                'slug' => 'basketball',
                'route' => 'admin.sportdata',
                'logo' => 'fas fa-basketball-ball',
                'sub' => [
                    [
                        'name' => 'indexTeam',
                        'title' => 'Team',
                        'slug' => 'basketball',
                        'route' => 'admin.sportdata.listTeam',
                    ],
                    [
                        'name' => 'indexMatch',
                        'title' => 'Match',
                        'slug' => 'basketball',
                        'route' => 'admin.sportdata.listMatch',
                    ],
                ],
            ],
            [
                'title' => 'Baseball',
                'slug' => 'baseball',
                'route' => 'admin.sportdata',
                'logo' => 'fas fa-baseball-ball',
                'sub' => [
                    [
                        'name' => 'indexTeam',
                        'title' => 'Team',
                        'slug' => 'baseball',
                        'route' => 'admin.sportdata.listTeam',
                    ],
                    [
                        'name' => 'indexMatch',
                        'title' => 'Match',
                        'slug' => 'baseball',
                        'route' => 'admin.sportdata.listMatch',
                    ],
                ],
            ],
            [
                'title' => 'Tennis',
                'slug' => 'tennis',
                'route' => 'admin.sportdata',
                'logo' => 'fas fa-tv',
                'sub' => [
                    [
                        'name' => 'indexTeam',
                        'title' => 'Team',
                        'slug' => 'tennis',
                        'route' => 'admin.sportdata.listTeam',
                    ],
                    [
                        'name' => 'indexMatch',
                        'title' => 'Match',
                        'slug' => 'tennis',
                        'route' => 'admin.sportdata.listMatch',
                    ],
                ],
            ],
            [
                'title' => 'UFC',
                'slug' => 'ufc',
                'route' => 'admin.football',
                'logo' => 'fas fa-tv',
                'sub' => [
                    [
                        'name' => 'indexTeam',
                        'title' => 'Team',
                        'slug' => 'ufc',
                        'route' => 'admin.sportdata.listTeam',
                    ],
                    [
                        'name' => 'indexMatch',
                        'title' => 'Match',
                        'slug' => 'ufc',
                        'route' => 'admin.sportdata.listMatch',
                    ],
                ],
            ],
            [
                'title' => 'Badminton',
                'slug' => 'badminton',
                'route' => 'admin.sportdata',
                'logo' => 'fas fa-tv',
                'sub' => [
                    [
                        'name' => 'indexTeam',
                        'title' => 'Team',
                        'slug' => 'badminton',
                        'route' => 'admin.sportdata.listTeam',
                    ],
                    [
                        'name' => 'indexMatch',
                        'title' => 'Match',
                        'slug' => 'badminton',
                        'route' => 'admin.sportdata.listMatch',
                    ],
                ],
            ],
        ];

        return json_decode(json_encode($menuList));
    }
}

if (!function_exists('refreshBlockData')) {
    function refreshBlockData()
    {
        file_get_contents('https://api.mirrorstream.xyz/charlie/football.php');
        file_get_contents('https://api.mirrorstream.xyz/charlie/basketball.php');
        file_get_contents('https://api.mirrorstream.xyz/delta/api.php');
    }
}

if (!function_exists('getBlockMenu')) {
    function getBlockMenu()
    {
        $menuList = [
            [
                'name' => 'blockMatch',
                'title' => 'Block Match',
                'slug' => 'match',
                'route' => 'admin.sportdata.block',
                'logo' => 'fas fa-futbol',
            ],
            [
                'name' => 'blockLiga',
                'title' => 'Block Liga',
                'slug' => 'liga',
                'route' => 'admin.sportdata.block',
                'logo' => 'fas fa-futbol',
            ],
        ];

        return json_decode(json_encode($menuList));
    }
}

if (!function_exists('setActiveMenu')) {
    function setActiveMenu($name = null, $action = null)
    {
        session()->put('active_menu_name', $name);
        session()->put('active_menu_action', $action);
    }
}
if (!function_exists('setTitle')) {
    function setTitle($title)
    {
        session()->put('title', $title.' - '.env('APP_NAME'));
    }
}
