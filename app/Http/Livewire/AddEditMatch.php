<?php

namespace App\Http\Livewire;

use App\Models\SportMatch;
use Livewire\Component;

class AddEditMatch extends Component
{
    public $idData;

    public $sportname;
    public $editData;
    public $streamList = [];

    public $home = '';
    public $away = '';
    public $liga = '';
    public $isRequest = false;

    public $homeError;
    public $awayError;
    public $ligaError;

    public $saveData;

    public function mount()
    {
        if ('new' !== $this->idData) {
            $editData = SportMatch::find($this->idData);

            $this->home = $editData->home;
            $this->away = $editData->away;
            $this->liga = $editData->liga;
            $this->isRequest = $editData->is_request;
            if (is_array($editData->sources)) {
                $this->streamList = $editData->sources;
            } else {
                $this->streamList = json_decode($editData->sources);
            }
        }
    }

    public function save()
    {
        if ('' === $this->liga) {
            $this->ligaError = 'Input Liga Terlebih Dahulu';
        } else {
            $this->ligaError = null;
        }

        if ('' === $this->away) {
            $this->awayError = 'Pilih Away Team Terlebih Dahulu';
        } else {
            $this->awayError = null;
        }

        if ('' === $this->home) {
            $this->homeError = 'Pilih Home Team Terlebih Dahulu';
        } else {
            $this->homeError = null;
        }

        $lastPosition = SportMatch::where('sport', $this->sportname)->max('position');
        if (null !== $lastPosition) {
            $lastPosition = $lastPosition + 1;
        } else {
            $lastPosition = 1;
        }

        $this->saveData = [
            'liga' => $this->liga,
            'home' => $this->home,
            'away' => $this->away,
            'sport' => $this->sportname,
            'is_request' => $this->isRequest,
            'sources' => $this->streamList,
            'position' => $lastPosition,
        ];

        if (null === $this->ligaError && null === $this->homeError && null === $this->awayError) {
            if ('new' === $this->idData) {
                $save = saveMatch($this->saveData);
                if (true === $save) {
                    return redirect(route('admin.sportdata.listMatch', ['sport' => $this->sportname]));
                }
            } else {
                $update = updateMatch($this->idData, $this->saveData);
                if (true === $update) {
                    return redirect(route('admin.sportdata.listMatch', ['sport' => $this->sportname]));
                }
            }

            saveSport($this->sportname);
        }
    }

    public function addStream()
    {
        array_push($this->streamList, [
            'type' => 'HLS',
            'source' => '',
        ]);
    }

    public function removeSource($key)
    {
        unset($this->streamList[$key]);
        $this->streamList = array_values($this->streamList);
    }

    public function render()
    {
        $teamList = getTeamList($this->sportname);

        return view('livewire.add-edit-match', [
            'teamList' => $teamList,
        ]);
    }
}
