<?php

namespace App\Http\Livewire;

use App\Models\LinkAgenPromo;
use Livewire\Component;

class AddAgenPromo extends Component
{
    public $editId;

    public $judul;
    public $kalimat;
    public $image;
    public $urls;

    public function mount()
    {
        if ('new' !== $this->editId) {
            $data = LinkAgenPromo::find($this->editId);
            $this->judul = $data->judul;
            $this->kalimat = $data->kalimat;
            $this->image = $data->image;
            $this->urls = $data->urls;
        } else {
            $this->urls = [];
        }
    }

    public function addLink()
    {
        array_push($this->urls, '');
    }

    public function removeLink($index)
    {
        unset($this->urls[$index]);
        $this->urls = array_values($this->urls);
    }

    public function render()
    {
        return view('livewire.add-linkagenpromo', );
    }

    public function save()
    {
        try {
            $save = ('new' !== $this->editId) ? LinkAgenPromo::find($this->editId) : new LinkAgenPromo(['id' => Str::random(6)]);
            $save->judul = $this->judul;
            $save->kalimat = $this->kalimat;
            $save->image = $this->image;
            $save->urls = $this->urls;
            $save->save();

            $data = LinkAgenPromo::orderBy('position')->get();
            saveJson('link-agenpromo', $data);

            return redirect(route('admin.linkagenpromo'));
        } catch (\Throwable $th) {
            //throw $th;
        }
    }
}
