<?php

namespace App\Http\Livewire;

use App\Models\TipsterArea;
use Livewire\Component;

class TipsterareaAddEdit extends Component
{

    public $cardHeaderTitle;
    public $otherId = null;
    public $betList = [];
    public $chList = [];


    public $betArraySample = [
        'text' => 'New Bet data',
        'color' => '#000000',
    ];
    public $channelArraySample = [
        'type' => 'hls',
        'source' => "https://",
    ];





    // add channel to array
    public function addChannel()
    {
        array_push($this->chList, $this->channelArraySample);
    }

    // add bet to array
    public function addBet()
    {
        array_push($this->betList, $this->betArraySample);
    }

    // remove channel from array bey key
    public function removeChannel($key)
    {
        unset($this->chList[$key]);
        $this->chList = array_values($this->chList);
    }

    // remove bet from array by key
    public function removeBet($key)
    {
        unset($this->betList[$key]);
        $this->betList = array_values($this->betList);
    }


    public function mount()
    {
      
        $dataAwal = TipsterArea::find('master');

        if ($dataAwal === null) {
            $dataAwal = new TipsterArea(['id' => 'master']);
            $dataAwal->channel  =  [$this->channelArraySample];
            $dataAwal->bet  =  [$this->betArraySample];
            $dataAwal->save();
            $dataAwal = TipsterArea::find('master');
        }
        $this->chList = $dataAwal->channel;
        $this->betList = $dataAwal->bet;
    }

    public function save()
    {

        $save =   TipsterArea::firstOrNew(['id' => 'master']);
        $save->bet =  $this->betList;
        $save->channel = $this->chList;
        $save->save();

        saveJson('tipster-area', $save);
        $this->emit('simplealert', [
            'text' => "Tipster Area Data Updated"
        ]);
    }
    public function render()
    {
        setTitle('Tipster Area');
        setActiveMenu('tipsterarea');
        return view('livewire.tipsterarea-add-edit')->extends('admin.layout')->section('content-body');
    }
}
