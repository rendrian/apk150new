<?php

namespace App\Http\Livewire;

use App\Models\LinkBetgratis;
use Livewire\Component;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AddLinkbetgratis extends Component
{
    public $editId;

    public $judul, $kalimat, $image, $urls;

    public function mount()
    {
        if ($this->editId !== 'new') {
            $data = LinkBetgratis::find($this->editId);
            $this->judul = $data->judul;
            $this->kalimat = $data->kalimat;
            $this->image = $data->image;
            $this->urls = $data->urls;
        } else {

            $this->urls = [];
        }
    }

    public function addLink()
    {
        array_push($this->urls, "");
    }
    public function removeLink($index)
    {
        unset($this->urls[$index]);
        $this->urls = array_values($this->urls);
    }


    public function render()
    {

        return view('livewire.add-linkbetgratis',);
    }

    public function save()
    {
        try {

            $save = ($this->editId !== 'new') ? LinkBetgratis::find($this->editId) : new LinkBetgratis(['id' => Str::random(6)]);
            $save->judul = $this->judul;
            $save->kalimat = $this->kalimat;
            $save->image = $this->image;
            $save->urls = $this->urls;
            $save->save();

            $data   = LinkBetgratis::orderBy('position')->get();
            saveJson('link-betgratis', $data);
            return redirect(route('admin.linkbetgratis'));
        } catch (\Throwable $th) {
            //throw $th;
        }
    }
}
