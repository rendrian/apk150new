<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\SportMatch;

class MatchController extends Controller
{
    public function listMatch($sport, Request $request)
    {
        setActiveMenu($sport, 'indexMatch');
        setTitle(ucwords($sport) . ' Match');
        $data = SportMatch::with(['homedata', 'awaydata'])->where('sport', $sport)->orderBy('position', 'asc')->get();


        return view('admin.match.index', [
            'sportname' => $sport,
            'data' => $data
        ]);
    }

    public function editMatch($sport, $id)
    {
        setActiveMenu($sport, 'addMatch');
        setTitle('Edit ' . ucwords($sport) . ' Match');

        return view('admin.match.add', [
            'sportname' => $sport,
            'id' => $id
        ]);
    }

    public function addMatch($sport, Request $request)
    {
        setActiveMenu($sport, 'addMatch');
        setTitle('Add ' . ucwords($sport) . ' Match');
        return view('admin.match.add', [
            'sportname' => $sport,
            'id' => 'new'
        ]);
    }

    public function deleteMatch($sportname, $id)
    {
        setTitle('Delete ' . ucwords($sportname) . ' Match');
        $data = SportMatch::with(['homedata', 'awaydata'])->find($id);
        return view('admin.match.delete-confirm', ['data' => $data]);
    }

    public function doDeleteMatch($sport, $id)
    {

        $delete = destroyMatch($id, $sport);

        if ($delete === true) {
            return redirect(route('admin.sportdata.listMatch', [$sport]))->with('notice', 'Data delete successfully');
        } else {
            return redirect()->back()->with('error', "Data delete error, contact admin!!!");
        }
    }
}
