<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TeamController extends Controller
{
    public function listTeam($sport, Request $request)
    {
        setActiveMenu($sport, 'indexTeam');
        setTitle(ucwords($sport) . ' Team');
        return view('admin.team.index', [
            'sportname' => $sport
        ]);
    }

    public function addTeam($sport, Request $request)
    {
        setActiveMenu($sport, 'addTeam');
        setTitle('Add ' . ucwords($sport) . ' Team');
        return view('admin.team.add', [
            'sportname' => $sport
        ]);
    }

    public function editTeam($sport, $id, Request $request)
    {
        setActiveMenu($sport, 'addTeam');
        setTitle('Edit ' . ucwords($sport) . ' Team');
        $data = DB::table('teams')
            ->where('sport', $sport)
            ->where('id', $id)
            ->first();

        return view('admin.team.add', [
            'sportname' => $sport,
            'data' => $data
        ]);
    }

    public function doEditTeam(Request $request)
    {

        if (isset($request->id)) {
            $data = $request->only('id', 'name', 'logo', 'sport');
            $update = updateTeam($data);
            if ($update === true) {
                return redirect(route('admin.sportdata.listTeam', [$request->sport]))->with('notice', 'Data updated successfully');
            } else {
                return redirect()->back()->with('error', "Data save error, contact admin!!!");
            }
        } else {
            $data = $request->only('name', 'logo', 'sport');
            $update = addTeam($data);
            if ($update === true) {
                return redirect(route('admin.sportdata.listTeam', [$request->sport]))->with('notice', 'Data added successfully');
            } else {
                return redirect()->back()->with('error', "Data add error, contact admin!!!");
            }
        }
    }

    public function deleteTeam($sport, $id, Request $request)
    {
        setTitle('Delete ' . ucwords($sport) . ' Team');
        $data =  getTeam($id, $sport);
        return view('admin.team.delete-confirm', ['data' => $data]);
        // $data = $request->only('id', 'sport');
    }

    public function doDeleteTeam($sport, $id)
    {
        $delete = destroyTeam($id, $sport);
        if ($delete === true) {
            return redirect(route('admin.sportdata.listTeam', [$sport]))->with('notice', 'Data delete successfully');
        } else {
            return redirect()->back()->with('error', "Data delete error, contact admin!!!");
        }
    }
}
