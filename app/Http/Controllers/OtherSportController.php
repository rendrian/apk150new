<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\OtherSports;

class OtherSportController extends Controller
{
    public function index()
    {
        setActiveMenu('othersports');
        setTitle('Other Sports');


        $otherSportData = OtherSports::orderBy('position', 'asc')->get();

        return view('admin.othersport.index', [
            'sportlist' => $otherSportData
        ]);
    }

    public function delete($id)
    {
        $data = OtherSports::find($id);
        return view('admin.othersport.confirm-delete', [
            'data' => $data
        ]);
    }

    public function doDelete($otherId)
    {
        $delete = destroyOtherSport($otherId);
        // dd($delete);

        if ($delete === true) {
            return redirect(route('admin.othersports.index'))->with('notice', 'Data delete successfully');
        } else {
            return redirect()->back()->with('error', "Data delete error, contact admin!!!");
        }
    }
}
