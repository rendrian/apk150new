<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;

class BlocksController extends Controller
{

    public function block($type)
    {
        setActiveMenu('block' . ucwords($type), $type);
        setTitle("Block " . ucwords($type));
        $data  =  getBlockListData($type);
        return view('admin.blocks.index', [
            'type' => $type,
            'data' => $data
        ]);
    }

    public function addBlock($type, Request $request)
    {
        setTitle("Add Block " . ucwords($type));

        if (isset($request->id)) {
            $data = getBlockDataById($type, $request->id);
        } else {
            $data = [];
        }
        return view('admin.blocks.add', [
            'type' => $type,
            'data' => $data
        ]);
    }

    public function doAddBlock($type, Request $request)
    {

        try {
            // DB::table('block_' . $type)->where('id', $request->id)->update(['string' => $request->string]);
            DB::table('block_' . $type)->updateOrInsert(['id' => $request->id], ['string' => $request->string]);
            writeBlock($type);
            refreshBlockData();
            return redirect(route('admin.sportdata.block', $type));
        } catch (\Throwable $th) {
            Log::error("Insert Block $type, msg : " . $th->getMessage());
        }
    }


    public function deleteBlock($type, Request $request)
    {
        setTitle("Delete Block " . ucwords($type));
        $data = getBlockDataById($type, $request->id);
        return view('admin.blocks.delete', ['data' => $data, 'type' => $type]);
    }

    public function doDeleteBlock($type, Request $request)
    {
        $delete = destroyBlock($type, $request->id);

        if ($delete === true) {
            refreshBlockData();
            return redirect(route('admin.sportdata.block', [$type]))->with('notice', 'Data delete successfully');
        } else {
            return redirect()->back()->with('error', "Data delete error, contact admin!!!");
        }
    }
}
