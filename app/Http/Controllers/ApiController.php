<?php

namespace App\Http\Controllers;

use App\Models\LinkAgenPromo;
use App\Models\LinkBetgratis;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    public static function cronDeleteLive()
    {
        // first delete all data
        DB::table('match')->where('is_request', false)->delete();

        // save all sport
        $sports = [
            'football',
            'basketball',
            'baseball',
            'tennis',
            'ufc',
            'badminton',
        ];

        foreach ($sports as $sport) {
            saveSport($sport);
        }
    }

    public function changeMatchPosition(Request $request)
    {
        try {
            foreach ($request->data as $key => $value) {
                DB::table('match')->where('id', $value)->update(['position' => $key + 1]);
            }

            saveSport($request->sportname);

            return response([
                'success' => true,
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
            ], 200);
        }
    }

    public function changeLinkBetgratisPosition(Request $request)
    {
        try {
            foreach ($request->data as $key => $value) {
                DB::table('link_betgratis')->where('id', $value)->update(['position' => $key + 1]);
            }

            $data = LinkBetgratis::orderBy('position')->get();
            saveJson('link-betgratis', $data);

            return response([
                'success' => true,
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
            ], 200);
        }
    }

    public function changeLinkAgenPromoPosition(Request $request)
    {
        try {
            foreach ($request->data as $key => $value) {
                DB::table('link_agenpromo')->where('id', $value)->update(['position' => $key + 1]);
            }

            $data = LinkAgenPromo::orderBy('position')->get();
            saveJson('link-agenpromo', $data);

            return response([
                'success' => true,
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
            ], 200);
        }
    }

    public function changeOtherSportPosition(Request $request)
    {
        try {
            foreach ($request->data as $key => $value) {
                DB::table('other_sports')->where('id', $value)->update(['position' => $key + 1]);
            }

            writeOtherSport();

            return response([
                'success' => true,
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
            ], 200);
        }
    }
}
