<?php

use App\Http\Controllers\AdminpanelController;
use App\Http\Controllers\AuthxController;
use App\Http\Controllers\BlocksController;
use App\Http\Controllers\LinkAgenPromoC;
use App\Http\Controllers\LinkBetgratisController;
use App\Http\Controllers\MatchController;
use App\Http\Controllers\OtherSportController;
use App\Http\Controllers\TeamController;
use App\Http\Controllers\TipsterareaController;
use App\Http\Livewire\OtherSportAddEdit;
use App\Http\Livewire\TipsterareaAddEdit;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['admin'])->group(function () {
    Route::prefix('panel')->group(function () {
        Route::get('/', [AdminpanelController::class, 'index'])->name('admin.index');

        route::get('{sport?}/add-match', [MatchController::class, 'addMatch'])->name('admin.sportdata.addMatch');
        route::get('{sport?}/edit-match/{id?}', [MatchController::class, 'editMatch'])->name('admin.sportdata.editMatch');
        route::post('/match-action', [MatchController::class, 'doEditMatch'])->name('admin.sportdata.doEditMatch');
        route::get('{sport?}/list-match', [MatchController::class, 'listMatch'])->name('admin.sportdata.listMatch');

        route::get('{sport?}/delete-match/{id?}', [MatchController::class, 'deleteMatch'])->name('admin.sportdata.deleteMatch');
        route::post('{sport?}/delete-match/{id?}', [MatchController::class, 'doDeleteMatch'])->name('admin.sportdata.doDeleteMatch');

        route::get('{sport?}/add-team', [TeamController::class, 'addTeam'])->name('admin.sportdata.addteam');

        // team
        route::get('{sport?}/edit-team/{id?}', [TeamController::class, 'editTeam'])->name('admin.sportdata.editTeam');

        route::get('{sport?}/delete-team/{id?}', [TeamController::class, 'deleteTeam'])->name('admin.sportdata.deleteTeam');
        route::post('{sport?}/delete-team/{id?}', [TeamController::class, 'doDeleteTeam'])->name('admin.sportdata.doDeleteTeam');
        route::post('/team-action', [TeamController::class, 'doEditTeam'])->name('admin.sportdata.doEditTeam');

        route::get('{sport?}/list-team', [TeamController::class, 'listTeam'])->name('admin.sportdata.listTeam');
        // route::get('{sport?}', [TeamController::class, 'index'])->name('admin.sportdata.teamList');

        Route::get('/block/{slug}', [BlocksController::class, 'block'])->name('admin.sportdata.block');
        Route::get('/block/{slug}/add', [BlocksController::class, 'addBlock'])->name('admin.sportdata.block.add');
        Route::get('/block/{slug}/delete', [BlocksController::class, 'deleteBlock'])->name('admin.sportdata.block.delete');
        Route::post('/block/{slug}/delete', [BlocksController::class, 'doDeleteBlock'])->name('admin.sportdata.block.delete.do');
        Route::post('/block/{slug}/add', [BlocksController::class, 'doAddBlock'])->name('admin.sportdata.block.add.do');

        Route::get('link-betgratis', [LinkBetgratisController::class, 'index'])->name('admin.linkbetgratis');
        Route::get('link-betgratis/add', [LinkBetgratisController::class, 'addLink'])->name('admin.linkbetgratis.add');
        route::get('link-betgratis/delete', [LinkBetgratisController::class, 'confirmDelete'])->name('admin.sportdata.deleteLinkBetgratis');
        route::post('link-betgratis/{id}/delete', [LinkBetgratisController::class, 'doDelete'])->name('admin.sportdata.do.deleteLinkBetgratis');

        // link agenpromo
        Route::get('link-agenpromo', [LinkAgenPromoC::class, 'index'])->name('admin.linkagenpromo');
        Route::get('link-agenpromo/add', [LinkAgenPromoC::class, 'addLink'])->name('admin.linkagenpromo.add');
        route::get('link-agenpromo/delete', [LinkAgenPromoC::class, 'confirmDelete'])->name('admin.sportdata.deletelinkagenpromo');
        route::post('link-agenpromo/{id}/delete', [LinkAgenPromoC::class, 'doDelete'])->name('admin.sportdata.do.deletelinkagenpromo');

        Route::prefix('other-sports')->group(function () {
            route::get('', [OtherSportController::class, 'index'])->name('admin.othersports.index');
            route::get('add', OtherSportAddEdit::class)->name('admin.othersports.add');
            route::get('edit/{otherId}', OtherSportAddEdit::class)->name('admin.othersports.edit');
            route::get('delete/{otherId}', [OtherSportController::class, 'delete'])->name('admin.othersports.delete');
            route::post('delete/{otherId}', [OtherSportController::class, 'doDelete'])->name('admin.othersport.doDelete');
        });

        Route::prefix('tipster-area')->group(function () {
            setActiveMenu('tipsterarea');
            setTitle('Tipster Area');
            // Route::get('', [TipsterareaController::class, 'index'])->name('admin.tipsterarea.index');
            Route::get('/', TipsterareaAddEdit::class)->name('admin.tipsterarea.index');
            Route::get('/edit/{otherId}', TipsterareaAddEdit::class)->name('admin.tipsterarea.edit');
            Route::get('delete/{otherId}', [TipsterareaController::class, 'delete'])->name('admin.tipsterarea.delete');
            Route::post('delete/{otherId}', [TipsterareaController::class, 'doDelete'])->name('admin.tipsterarea.doDelete');
        });
    });
});

Route::prefix('auth')->group(function () {
    Route::get('login', [AuthxController::class, 'login'])->name('show.login');
    Route::post('login', [AuthxController::class, 'doLogin'])->name('do.login');
    Route::any('logout', [AuthxController::class, 'logout'])->name('do.logout');
});
