<div>

    <div class="row">
      <div class="col-12">
  
      </div>
  
      <div class="col-12 col-md-6 col-lg-6">
  
        <div class="card">
          <div class="card-header">
            <h4>Match Info</h4>
            <div class="card-header-action">
              <button class="btn btn-icon btn-plus btn-primary" wire:click="addLink"> <i class="fas fa-plus"></i>
                Stream</button>
  
            </div>
          </div>
          <div class="card-body">
            <label for="liga">Liga</label>
            <div class="form-group ">
              <input type="hidden" class="form-control" wire:model="editId">
            </div>
            <div class="form-group ">
              <input type="text" class="form-control" wire:model="judul" placeholder="judul">
            </div>
            <div class="form-group ">
              <input type="text" class="form-control" wire:model="image" placeholder="URL Image">
            </div>
            <div class="form-group ">
              <input type="text" class="form-control" wire:model="kalimat" placeholder="Kalimat">
            </div>
  
  
  
  
  
            <div class="form-group">
              <input class="form-control btn btn-primary" type="submit" value="SAVE" wire:click="save">
            </div>
  
          </div>
        </div>
      </div>
      <div class="col-12 col-md-6 col-lg-6">
  
        @foreach ($urls as $key => $url)
          <div class="card">
            <div class="card-header">
              <h4>Link {{ $key + 1 }}</h4>
              <div class="card-header-action">
                <button class="btn btn-icon btn-plus btn-danger" wire:click="removeLink({{ $key }})"> <i
                    class="fas fa-minus"></i></button>
  
              </div>
            </div>
            <div class="card-body">
              <div class="form-group">
                <input type="text" class="form-control" wire:model="urls.{{ $key }}" value="{{ $url }}">
              </div>
            </div>
          </div>
  
        @endforeach
      </div>
    </div>
  </div>
  
  
  
  
  
  
  
  
  </div>
  
  
  