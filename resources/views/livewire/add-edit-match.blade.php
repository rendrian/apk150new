<div>

  <div class="row">
    <div class="col-12">

      @if ($awayError !== null)
        <div class="alert alert-danger alert-dismissible show fade">
          <div class="alert-body">
            <button class="close" data-dismiss="alert">
              <span>&times;</span>
            </button>
            {{ $awayError }}

          </div>
        </div>
      @endif
      @if ($ligaError !== null)
        <div class="alert alert-danger alert-dismissible show fade">
          <div class="alert-body">
            <button class="close" data-dismiss="alert">
              <span>&times;</span>
            </button>
            {{ $ligaError }}

          </div>
        </div>
      @endif
      @if ($homeError !== null)
        <div class="alert alert-danger alert-dismissible show fade">
          <div class="alert-body">
            <button class="close" data-dismiss="alert">
              <span>&times;</span>
            </button>
            {{ $homeError }}

          </div>
        </div>
      @endif

    </div>


    <div class="col-12 col-md-6 col-lg-6">

      <div class="card">
        <div class="card-header">
          <h4>Match Info</h4>
          <div class="card-header-action">
            <button class="btn btn-icon btn-plus btn-primary" wire:click="addStream"> <i class="fas fa-plus"></i>
              Stream</button>

          </div>
        </div>
        <div class="card-body">
          <label for="liga">Liga</label>
          <div class="form-group ">
            <input type="text" class="form-control " name="liga" id="liga" wire:model="liga" autocomplete="off">
          </div>

          <div class="form-group" wire:ignore>
            <label>Home Team</label>
            <select class="form-control " id="home-team-select">
              <option value="null">Select Home Team</option>
              @foreach ($teamList as $team)
                <option value="{{ $team->id }}" {{ $home === $team->id ? 'selected' : '' }}>{{ $team->name }}
                </option>
              @endforeach
            </select>

          </div>

          <div class="form-group" wire:ignore>
            <label>Away Team</label>
            <select class="form-control " id="away-team-select">
              <option value="null">Select Home Team</option>
              @foreach ($teamList as $team)
                <option value="{{ $team->id }}" {{ $away === $team->id ? 'selected' : '' }}>{{ $team->name }}
                </option>
              @endforeach
            </select>


          </div>


          <label class="custom-switch mt-2">
            <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input" wire:model="isRequest">
            <span class="custom-switch-indicator"></span>
            <span class="custom-switch-description">Server Backup</span>
          </label>
        </div>


        <div class="form-group">
          <input class="form-control btn btn-primary" type="submit" value="SAVE" wire:click="save">
        </div>

      </div>
    </div>

    <div class="col-12 col-md-6 col-lg-6">
      @foreach ($streamList as $key => $stream)
        <div class="card">
          <div class="card-header">
            <h4>Source {{ $key + 1 }}</h4>
            <div class="card-header-action">
              <button class="btn btn-icon btn-minus btn-danger" wire:click="removeSource({{ $key }})"> <i
                  class="fas fa-minus"></i>
                Source</button>

            </div>

          </div>
          <div class="card-body">
            <div class="form-row">
              <div class="form-group col-md-3">
                <label for="inputEmail4">Type</label>
                <select name="home" class="form-control" wire:model="streamList.{{ $key }}.type">
                  <option value="hls">HLS</option>
                  <option value="iframe">IFRAME</option>
                  <option value="mpd">MPD</option>
                  
                </select>
              </div>
              <div class="form-group col-md-9">
                <label for="inputPassword4">Source</label>
                <input type="text" class="form-control" placeholder="Source {{ $key }}"
                  wire:model="streamList.{{ $key }}.source">
              </div>
            </div>
          </div>


        </div>
      @endforeach
    </div>
  </div>
</div>








</div>

@push('js')

  <script src="/assets/modules/select2/dist/js/select2.full.min.js"></script>
  {{-- <script src="/assets/modules/jquery-selectric/jquery.selectric.min.js"></script> --}}

  <script>
    $(document).ready(function() {
      $('#home-team-select').select2();
      $('#home-team-select').on('change', function(e) {
        var data = $('#home-team-select').select2("val");
        @this.set('home', data);
      });
      $('#away-team-select').select2();
      $('#away-team-select').on('change', function(e) {
        var data = $('#away-team-select').select2("val");
        @this.set('away', data);
      });
    });

  </script>


@endpush
