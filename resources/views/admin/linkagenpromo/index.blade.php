@extends('admin.layout')
@section('content-header')
  <h1>Link Agen Promo</h1>
@endsection

@section('content-body')
  <div class="row">
    <div class="col-12 col-md-6 col-lg-12">
      @if (session('notice'))
        <div class="alert alert-success alert-dismissible show fade">
          <div class="alert-body">
            <button class="close" data-dismiss="alert">
              <span>&times;</span>
            </button>
            {{ session('notice') }}
          </div>
        </div>
      @endif
      <div class="card">
        <div class="card-header">
          <h4></h4>
          <div class="card-header-action">
            <a href="{{ route('admin.linkagenpromo.add') }}" class="btn btn-icon btn-primary" href="#"><i
                class="fas fa-plus"></i> ADD</a>
            <button class="btn btn-icon btn-success" id="save-linkagenpromo-position"><i class="fas fa-plus"></i>
              SAVE</button>

          </div>
        </div>
        <div class="card-body">
          <table class="table table-striped table-dark">
            <thead>
              <tr>
                <th scope="col">Judul</th>

                <th scope="col" class="text-center">Action</th>
              </tr>
            </thead>
            <tbody id="linkagenpromo-list">
              @foreach ($data as $link)

                <tr id="{{ $link->id }}">
                  <td>{{ $link->judul }}</td>


                  <td class="text-center">
                    <a href="{{ route('admin.linkagenpromo.add', ['id' => $link->id]) }}"
                      class="btn btn-sm btn-icon icon-left btn-primary"><i class="far fa-edit"></i> Edit</a>
                    &nbsp;
                    <a href="{{ route('admin.sportdata.deletelinkagenpromo', ['id' => $link->id]) }}"
                      class="btn btn-sm btn-icon icon-left btn-danger" id="delete-team" "><i class=" fa fa-times"></i>
                      Delete</a>

                  </td>
                </tr>
              @endforeach

            </tbody>
          </table>


        </div>
      </div>
    </div>
  @endsection


  @push('css')
    <link rel="stylesheet" href="/assets/modules/datatables/datatables.min.css">
    <link rel="stylesheet" href="/assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="/assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css">

  @endpush

  @push('js')
    <!-- JS Libraies -->
    <script src="/assets/modules/datatables/datatables.min.js"></script>
    <script src="/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script src="/assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js"></script>
    <script src="/assets/modules/jquery-ui/jquery-ui.min.js"></script>
    <script src="/assets/modules/jquery-sortable-list/jquery-sortable-lists.min.js"></script>

    <!-- Page Specific JS File -->
    <script>
      $("#team-table").dataTable({
        "columnDefs": [{
          "sortable": true,
          "targets": [0]
        }]
      });

    </script>

  @endpush
