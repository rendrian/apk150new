@extends('admin.layout')
@section('content-header')
  <h1>Add Team {{ ucwords($sportname) }}</h1>
@endsection

@section('content-body')
  <div class="row">


    <div class="col-12 col-md-6 col-lg-6">
      @if (session('error'))
        <div class="alert alert-warning alert-dismissible show fade">
          <div class="alert-body">
            <button class="close" data-dismiss="alert">
              <span>&times;</span>
            </button>
            {{ session('error') }}
          </div>
        </div>
      @endif
      <div class="card">

        <div class="card-header">

        </div>
        <div class="card-body">

          <form action="{{ route('admin.sportdata.doEditTeam') }}" method="POST">
            @csrf
            <input type="hidden" name="id" value="{{ isset($data->id) ? $data->id : '' }}">
            <input type="hidden" name="sport" value="{{ isset($data->sport) ? $data->sport : $sportname }}">

            <div class="form-group">
              <label for="name">Name</label>
              <input class="form-control" type="text" id="name" name="name"
                value="{{ isset($data->name) ? $data->name : '' }}">
            </div>

            <div class="form-group">
              <label for="away">Logo</label>
              <input class="form-control" type="text" id="logo" name="logo"
                value="{{ isset($data->logo) ? $data->logo : '' }}">
            </div>
            <div class="form-group">
              <img src="{{ isset($data->logo) ? $data->logo : '' }}" loading="lazy" alt="" class="logo-preview"
                id="logo-preview" width="48" height="48">
            </div>
            <div class="form-group">
              <input class="form-control btn btn-primary" type="submit" value="SAVE">
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>




@endsection


@push('js')

  <script>
    $("#logo").change(function() {
      var imgUrl = $("#logo").val()
      $("#logo-preview").attr("src", imgUrl);
    });

  </script>


@endpush
