<li class="menu-header">Sport Center</li>
@foreach (getSportMenu() as $item)

  @isset($item->sub)
    <li class="dropdown {{ session('active_menu_name') === $item->slug ? 'active' : '' }}">
      <a href="#" class="nav-link has-dropdown"><i
          class="{{ isset($item->logo) ? $item->logo : 'fas fa-ellipsis-h' }}"></i>
        <span>{{ $item->title }}</span></a>
      @if (count($item->sub) > 0)
        <ul class="dropdown-menu">
          @foreach ($item->sub as $sub)
            <li class="{{ session('active_menu_action') === $sub->name ? 'active' : '' }}">
              <a href="{{ route($sub->route, ['sport' => $sub->slug]) }}">{{ $sub->title }}</a>
            </li>
          @endforeach
        </ul>
      @else

      @endif

    </li>
  @else
    <a href="#" class="nav-link has-dropdown"><i class="fas fa-ellipsis-h"></i> <span>{{ $item->title }}</span></a>

  @endisset
@endforeach


<li class="menu-header">Block Manager</li>
@foreach (getBlockMenu() as $item)

  @isset($item->sub)
    <li class="dropdown {{ route()->currentRouteName() }}">
      <a href="#" class="nav-link has-dropdown"><i
          class="{{ isset($item->logo) ? $item->logo : 'fas fa-ellipsis-h' }}"></i>
        <span>{{ $item->title }}</span></a>
      @if (count($item->sub) > 0)
        <ul class="dropdown-menu">
          @foreach ($item->sub as $sub)
            <li class="{{ session('active_menu_action') === $sub->name ? 'active' : '' }}">
              <a href="{{ route($sub->route, ['sport' => $sub->slug]) }}">{{ $sub->title }}</a>
            </li>
          @endforeach
        </ul>
      @else

      @endif

    </li>
  @else

    <li class="{{ session('active_menu_name') === $item->name ? 'active' : '' }}">
      <a href="{{ route($item->route, $item->slug) }}" class="nav-link "><i
          class="fas fa-ellipsis-h"></i><span>{{ $item->title }}</span></a>
    </li>



  @endisset
@endforeach

<li class=" {{ session('active_menu_name') === 'linkBetgratis' ? 'active' : '' }}">
  <a href="{{ route('admin.linkbetgratis') }}" class="nav-link "><i class="fas fa-ellipsis-h"></i><span>Link
      Betgratis</span></a>
</li>

<li class=" {{ session('active_menu_name') === 'linkagenpromo' ? 'active' : '' }}">
  <a href="{{ route('admin.linkagenpromo') }}" class="nav-link "><i class="fas fa-ellipsis-h"></i><span>Link
    AgenPromo</span></a>
</li>

<li class="menu-header">Other Sports</li>

<li class=" {{ session('active_menu_name') === 'othersports' ? 'active' : '' }}">
  <a href="{{ route('admin.othersports.index') }}" class="nav-link "><i class="fas fa-ellipsis-h"></i><span>Other
      Sports</span></a>
</li>
<li class="menu-header">Tipster Area</li>

<li class=" {{ session('active_menu_name') === 'tipsterarea' ? 'active' : '' }}">
  <a href="{{ route('admin.tipsterarea.index') }}" class="nav-link "><i class="fas fa-ellipsis-h"></i><span>Tipster
      Area</span></a>
</li>



</li>
