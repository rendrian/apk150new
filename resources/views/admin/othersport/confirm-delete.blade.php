@extends('admin.layout')
@section('content-header')

  <h1>Confirm Delete Other Sports</h1>
@endsection

@section('content-body')
  <div class="row justify-content-center">
    <div class="col-12 col-md-6 col-lg-6">
      <div class="card card-danger">
        <div class="card-header text-center ">
          <h4 class="m-auto">Other Sports Delete Confirmation</h4>
        </div>
        <div class="card-body">
          {{-- {{ json_encode($data) }} --}}
          <form action="{{ route('admin.othersport.doDelete', ['otherId' => $data->id]) }}" method="POST">
            @csrf
            <input type="hidden" name="id" value="{{ $data->id }}">
            <div class="text-center m-3">
              <h3>{{ $data->name }}</h3>

            </div>
            <div class="text-center m-3">
              <img src="{{ $data->logo }}" loading="lazy" alt="" width="50" height="50">

            </div>
            <div class="text-center mb-3">
              <caption><strong>{{ $data->name }}</strong></caption>
            </div>



            <input type="submit" class="btn btn-block btn-danger" value="DELETE">
            <a href="{{ url()->previous() }}" class="btn btn-block btn-success">CANCEL</a>

          </form>

        </div>
      </div>
    </div>
  </div>

@endsection
