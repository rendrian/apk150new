@extends('admin.layout')
@section('content-header')
  <h1>Add Match {{ ucwords($sportname) }}</h1>
@endsection

@section('content-body')


  @livewire('add-edit-match',['sportname'=>$sportname,
  'idData'=>$id
  ])




@endsection

@push('css')

  <link rel="stylesheet" href="/assets/modules/select2/dist/css/select2.min.css">

@endpush
