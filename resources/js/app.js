require('./bootstrap');



$(function() {

    // team delete button

    // delete button end
    var matchListSortableOptions = {
        update: function(event, ui) {

            var position = $("#match-list").sortable("toArray");
            var sportname = $("#save-match-position").data("attr")
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });

            var formData = {
                data: position,
                sportname: sportname
            }
            var ajaxurl = '/api/save-match-position';
            $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: formData,
                dataType: 'json',
                success: function(data) {
                    if (data.success == true) {
                        iziToast.success({
                            // title: 'Hello, world!',
                            message: 'Position Updated',
                            position: 'bottomRight'
                        });
                    }

                },
                error: function(data) {
                    console.log(data);
                }
            });
        }
    }

    $("#match-list").sortable(matchListSortableOptions)


    // save-match-position
    $("#save-match-position").on("click", function() {
        var position = $("#match-list").sortable("toArray");
        var sportname = $("#save-match-position").data("attr")
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });

        var formData = {
            data: position,
            sportname: sportname
        }
        var ajaxurl = '/api/save-match-position';
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.success == true) {
                    swal('Match Position Updated');

                    location.reload()
                }

            },
            error: function(data) {
                console.log(data);
            }
        });
    });





    //linkbetgratis

    $("#linkbetgratis-list").sortable({});

    $("#linkagenpromo-list").sortable({});
    
    $("#save-linkagenpromo-position").on("click", function() {
        var position = $("#linkagenpromo-list").sortable("toArray");
        var sportname = $("#save-linkagenpromo-position").data("attr")
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });

        var formData = {
            data: position,
            sportname: sportname
        }
        var ajaxurl = '/api/save-linkagenpromo-position';
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.success == true) {
                    swal('Link agenpromo Position Updated');
                    location.reload()
                }
            },
            error: function(data) {
                console.log(data);
            }
        });
    });
    


    // save-match-position
    $("#save-linkbetgratis-position").on("click", function() {
        var position = $("#linkbetgratis-list").sortable("toArray");
        var sportname = $("#save-linkbetgratis-position").data("attr")
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });

        var formData = {
            data: position,
            sportname: sportname
        }
        var ajaxurl = '/api/save-linkbetgratis-position';
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.success == true) {
                    swal('Link Betgratis Position Updated');
                    location.reload()
                }
            },
            error: function(data) {
                console.log(data);
            }
        });
    });
    // other sport table

    $("#save-othersport-position").on("click", function() {
        var position = $("#other-sport-list").sortable("toArray");

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });

        var formData = {
            data: position,

        }
        var ajaxurl = '/api/save-othersport-position';
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.success == true) {
                    swal('Other Sport Position Updated');
                    location.reload()
                }
            },
            error: function(data) {
                console.log(data);
            }
        });
    });





});